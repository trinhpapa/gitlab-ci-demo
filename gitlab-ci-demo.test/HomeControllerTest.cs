using gitlab_ci_demo.Controllers;
using Xunit;

namespace gitlab_ci_demo.test
{
   public class HomeControllerTest
   {
      [Fact]
      public void Sum_ReturnNumber4()
      {
         HomeController controller = new HomeController();
         var sumResult = controller.Sum(1, 3);
         const int test = 4;
         Assert.Equal(sumResult, test);
      }
   }
}