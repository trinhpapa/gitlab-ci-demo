﻿using gitlab_ci_demo.Models;
using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;

namespace gitlab_ci_demo.Controllers
{
   public class HomeController : Controller
   {
      public int Sum(int a, int b)
      {
         return (a + b);
      }

      public IActionResult Index()
      {
         return View();
      }

      public IActionResult Privacy()
      {
         return View();
      }

      [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
      public IActionResult Error()
      {
         return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
      }
   }
}